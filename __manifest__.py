# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Belajar Purchase Report',
    'version': '1.2',
    'category': 'Custom',
    'author': 'Niko Kusdiarto',
    'summary': 'Custom Module',
    'description': "Belajar Mengubah Bentuk Laporan.",
    'website': 'https://www.odoo.com/',
    'depends': ['purchase'],
    'data': [
        'report/purchase_quotation_report.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'LGPL-3',
}
